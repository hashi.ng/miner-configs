#!/bin/bash
cd /root/xmrigCC && chmod +x xmrigDaemon
cd /root/xmrigCC && chmod +x xmrigCCServer
cd /root/xmrigCC && chmod +x xmrigMiner

cd /root/xmrigCC && ./xmrigCCServer &
sleep 2
cd /root/xmrigCC && ./xmrigDaemon &


#Thresholds
HASH_TH=70
TIME_TH=60
AGE_TH=90

while true; do
echo sleeping15now  -------------------------------
	sleep 30
	HS=$(curl -H "Content-Type: application/json" -X GET http://admin:password@localhost:5555/admin/getClientStatusList | jq '.client_status_list[]' | jq -r '.client_status' | jq -r '.hashrate_medium')
	TX=$(curl -H "Content-Type: application/json" -X GET http://admin:password@localhost:5555/admin/getClientStatusList | jq '.client_status_list[]' | jq -r '.client_status' | jq -r '.last_status_update')
	AGE=$(curl -H "Content-Type: application/json" -X GET http://admin:password@localhost:5555/admin/getClientStatusList | jq '.client_status_list[]' | jq -r '.client_status' | jq -r '.uptime')
	EPOCH=$(date +%s)
	AGE=$(echo "$AGE / 1000" | bc -l | awk '{printf "%.0f", $0}')
	LAST_HASH=$(echo "$EPOCH - $TX" | bc -l | awk '{printf "%.0f", $0}' )
	HS=$(echo "$HS" | bc -l | awk '{printf "%.0f", $0}' )
	# 2 loops that can run independently of each other
	# HS & TX will never have an emtpy number (0 by default), hence no need for extra precautions

	# first, time
	# if there is a 30s+ difference between current time & time of last hash submitted, kill
	# check every 60s

	# second, hash
	# if hashrate is lower than the set threshold, kill
	# check every 60s


if [ $AGE -gt $AGE_TH ]; then
	  if [ "$HS" -lt "$HASH_TH" ]; then
	     /sbin/shutdown -h now
		fi

	 	if [ "$LAST_HASH" -gt "$TIME_TH" ]; then
	     /sbin/shutdown -h now

	  fi

	fi

done
